AMQ Kafka deployer
==================

Deploy Kafka Cluster Operator
=============================

1.  `git clone git@bitbucket.org:pabrahamsson/amq-kafka-deploy.git`
2.  `cd amq-kafka-deploy`
3.  `ansible-galaxy install -r requirements.yml -p roles`
4.  `ansible-playbook -i .applier roles/openshift-applier/playbooks/openshift-cluster-seed.yml -e filter_tags=operator`

The Kafka Cluster Operator will be deployed to `amq-kafka-operator` by default. Use `-e operator_namespace=<my-operator-namespace>` to alter the namespace.

Deploy a Kafka Cluster
======================

1.  `git clone git@bitbucket.org:pabrahamsson/amq-kafka-deploy.git`
2.  `cd amq-kafka-deploy`
3.  `ansible-galaxy install -r requirements.yml -p roles`
4.  `ansible-playbook -i .applier roles/openshift-applier/playbooks/openshift-cluster-seed.yml -e filter_tags=cluster`

The Kafka cluster will be deployed to `my-project` by default. Use `-e cluster_namespace=<my-cluster-namespace>` to alter the namespace.
